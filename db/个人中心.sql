-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.6.26 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 didi.p_buser 结构
CREATE TABLE IF NOT EXISTS `p_buser` (
  `user_num` int(50) NOT NULL AUTO_INCREMENT,
  `buser_num` int(50) DEFAULT '0',
  `buser_name` varchar(50) DEFAULT '0',
  `buser_date` varchar(50) DEFAULT '0',
  `remark` varchar(50) DEFAULT '0',
  PRIMARY KEY (`user_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='黑名单表';

-- 正在导出表  didi.p_buser 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_buser` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_buser` ENABLE KEYS */;


-- 导出  表 didi.p_costom 结构
CREATE TABLE IF NOT EXISTS `p_costom` (
  `cons_num` int(11) NOT NULL DEFAULT '1',
  `user_num` int(11) DEFAULT NULL,
  `cons_time` datetime DEFAULT NULL,
  `cons_name` varchar(50) DEFAULT NULL,
  `cons_mone` varchar(50) DEFAULT NULL,
  `cons_coin` varchar(50) DEFAULT NULL,
  `cons_stat` varchar(50) DEFAULT NULL,
  `cons_chan` varchar(50) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cons_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  didi.p_costom 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_costom` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_costom` ENABLE KEYS */;


-- 导出  表 didi.p_headpic 结构
CREATE TABLE IF NOT EXISTS `p_headpic` (
  `hp_num` int(11) NOT NULL AUTO_INCREMENT,
  `hp_date` varchar(50) DEFAULT NULL,
  `hp_url` varchar(100) DEFAULT NULL,
  `user_num` int(11) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hp_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='头像表';

-- 正在导出表  didi.p_headpic 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_headpic` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_headpic` ENABLE KEYS */;


-- 导出  表 didi.p_list 结构
CREATE TABLE IF NOT EXISTS `p_list` (
  `list_num` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(50) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`list_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='创作列表';

-- 正在导出表  didi.p_list 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_list` ENABLE KEYS */;


-- 导出  表 didi.p_listson 结构
CREATE TABLE IF NOT EXISTS `p_listson` (
  `list_num` int(11) DEFAULT NULL,
  `son_num` int(11) NOT NULL AUTO_INCREMENT,
  `son_name` varchar(50) DEFAULT NULL,
  `reamrk` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`son_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  didi.p_listson 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_listson` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_listson` ENABLE KEYS */;


-- 导出  表 didi.p_login 结构
CREATE TABLE IF NOT EXISTS `p_login` (
  `login_num` int(11) NOT NULL AUTO_INCREMENT,
  `login_time` datetime DEFAULT NULL,
  `login_ip` varchar(50) DEFAULT NULL,
  `login_loca` varchar(50) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`login_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  didi.p_login 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_login` ENABLE KEYS */;


-- 导出  表 didi.p_user 结构
CREATE TABLE IF NOT EXISTS `p_user` (
  `user_num` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `user_signa` varchar(50) DEFAULT NULL,
  `user_sex` varchar(50) DEFAULT NULL,
  `user_borth` varchar(50) DEFAULT NULL,
  `user_big` varchar(50) DEFAULT NULL,
  `user_inte` varchar(50) DEFAULT NULL,
  `user_coin` varchar(50) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表';

-- 正在导出表  didi.p_user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `p_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
