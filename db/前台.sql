-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.6.26 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 biubiubiu.adpicture 结构
CREATE TABLE IF NOT EXISTS `adpicture` (
  `adp_id` varchar(50) DEFAULT NULL COMMENT '图片id',
  `adp_num` varchar(50) DEFAULT NULL COMMENT '图片编号',
  `adp_url` varchar(50) DEFAULT NULL COMMENT '图片路径',
  `adp_remark` varchar(50) DEFAULT NULL COMMENT '图片备注',
  `adp_type` varchar(50) DEFAULT NULL COMMENT '所属类别'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广图片表';

-- 正在导出表  biubiubiu.adpicture 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `adpicture` DISABLE KEYS */;
/*!40000 ALTER TABLE `adpicture` ENABLE KEYS */;


-- 导出  表 biubiubiu.carouselpicture 结构
CREATE TABLE IF NOT EXISTS `carouselpicture` (
  `cp_id` varchar(50) DEFAULT NULL COMMENT '图片ID',
  `cp_num` varchar(50) DEFAULT NULL COMMENT '图片编号',
  `cp_url` varchar(50) DEFAULT NULL COMMENT '图片路径',
  `cp_remark` varchar(500) DEFAULT NULL COMMENT '图片备注',
  `cp_type` varchar(50) DEFAULT NULL COMMENT '所属类别',
  `cp_sort` varchar(50) DEFAULT NULL COMMENT '类别等级'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轮播图片表';

-- 正在导出表  biubiubiu.carouselpicture 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `carouselpicture` DISABLE KEYS */;
/*!40000 ALTER TABLE `carouselpicture` ENABLE KEYS */;


-- 导出  表 biubiubiu.label 结构
CREATE TABLE IF NOT EXISTS `label` (
  `label_id` varchar(50) DEFAULT NULL COMMENT '标签ID',
  `label_num` varchar(50) DEFAULT NULL COMMENT '标签编号',
  `label_name` varchar(50) DEFAULT NULL COMMENT '标签名',
  `label_desc` varchar(200) DEFAULT NULL COMMENT '标签描述',
  `label_createtime` varchar(50) DEFAULT NULL COMMENT '标签创建时间',
  `label_url` varchar(50) DEFAULT NULL COMMENT '链接'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签表';

-- 正在导出表  biubiubiu.label 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
/*!40000 ALTER TABLE `label` ENABLE KEYS */;


-- 导出  表 biubiubiu.labelvideo 结构
CREATE TABLE IF NOT EXISTS `labelvideo` (
  `lv_id` varchar(50) DEFAULT NULL COMMENT 'ID',
  `lv_num` varchar(50) DEFAULT NULL COMMENT '编号',
  `label_num` varchar(50) DEFAULT NULL COMMENT '标签编号',
  `video_num` varchar(50) DEFAULT NULL COMMENT '视频编号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签视频关联表';

-- 正在导出表  biubiubiu.labelvideo 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `labelvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `labelvideo` ENABLE KEYS */;


-- 导出  表 biubiubiu.labelvideotype 结构
CREATE TABLE IF NOT EXISTS `labelvideotype` (
  `lvt_id` varchar(50) DEFAULT NULL COMMENT 'ID',
  `lvt_num` varchar(50) DEFAULT NULL COMMENT '编号\n\n\n',
  `label_num` varchar(50) DEFAULT NULL COMMENT '标签编号',
  `vt_num` varchar(50) DEFAULT NULL COMMENT '分类编号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签分类表';

-- 正在导出表  biubiubiu.labelvideotype 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `labelvideotype` DISABLE KEYS */;
/*!40000 ALTER TABLE `labelvideotype` ENABLE KEYS */;


-- 导出  表 biubiubiu.newvideo 结构
CREATE TABLE IF NOT EXISTS `newvideo` (
  `nv_id` varchar(50) DEFAULT NULL COMMENT 'ID',
  `nv_num` varchar(50) DEFAULT NULL COMMENT '编号',
  `video_num` varchar(50) DEFAULT NULL COMMENT '视频编号',
  `nv_time` varchar(50) DEFAULT NULL COMMENT '新番时间',
  `nv_type` varchar(50) DEFAULT NULL COMMENT '新番分类'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新番表';

-- 正在导出表  biubiubiu.newvideo 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `newvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `newvideo` ENABLE KEYS */;


-- 导出  表 biubiubiu.video 结构
CREATE TABLE IF NOT EXISTS `video` (
  `video_id` varchar(50) DEFAULT NULL COMMENT '视频ID',
  `video_num` varchar(50) DEFAULT NULL COMMENT '视频编号',
  `video_title` varchar(50) DEFAULT NULL COMMENT '视频标题',
  `video_author` varchar(50) DEFAULT NULL COMMENT 'up主',
  `video_type` varchar(50) DEFAULT NULL COMMENT '视频分类',
  `video_introduction` varchar(500) DEFAULT NULL COMMENT '视频简介',
  `video_image` varchar(50) DEFAULT NULL COMMENT '图片',
  `video_url` varchar(50) DEFAULT NULL COMMENT '视频路径',
  `video_original` varchar(50) DEFAULT NULL COMMENT '原创',
  `video_time` varchar(50) DEFAULT NULL COMMENT '上传时间',
  `video_separation` varchar(50) DEFAULT NULL COMMENT '分集',
  `video_ins` varchar(50) DEFAULT NULL COMMENT '所属集',
  `video_views` varchar(50) DEFAULT NULL COMMENT '播放次数',
  `video_likes` varchar(50) DEFAULT NULL COMMENT '点赞次数',
  `video_coins` varchar(50) DEFAULT NULL COMMENT '硬币数',
  `video_favorites` varchar(50) DEFAULT NULL COMMENT '收藏数\n\n\n',
  `video_forwards` varchar(50) DEFAULT NULL COMMENT '转发数',
  `video_heats` varchar(50) DEFAULT NULL COMMENT '热度',
  `video_permission` varchar(50) DEFAULT NULL COMMENT '授权'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频表';

-- 正在导出表  biubiubiu.video 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;


-- 导出  表 biubiubiu.videoreview 结构
CREATE TABLE IF NOT EXISTS `videoreview` (
  `vr_id` varchar(50) DEFAULT NULL COMMENT '评论ID',
  `vr_num` varchar(50) DEFAULT NULL COMMENT '评论编号',
  `user_num` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `video_num` varchar(50) DEFAULT NULL COMMENT '视频编号',
  `vr_content` varchar(500) DEFAULT NULL COMMENT '评论内容',
  `vr_time` varchar(50) DEFAULT NULL COMMENT '评论时间',
  `vr_parentid` varchar(50) DEFAULT NULL COMMENT '父编号',
  `vr_likes` varchar(50) DEFAULT NULL COMMENT '点赞数',
  `vr_dislikes` varchar(50) DEFAULT NULL COMMENT '踩数',
  `vr_top` varchar(50) DEFAULT NULL COMMENT '置顶'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频评论表';

-- 正在导出表  biubiubiu.videoreview 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `videoreview` DISABLE KEYS */;
/*!40000 ALTER TABLE `videoreview` ENABLE KEYS */;


-- 导出  表 biubiubiu.videotype 结构
CREATE TABLE IF NOT EXISTS `videotype` (
  `vt_id` varchar(50) DEFAULT NULL COMMENT '分类ID',
  `vt_num` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `vt_name` varchar(50) DEFAULT NULL COMMENT '分类名',
  `vt_desc` varchar(200) DEFAULT NULL COMMENT '分类描述',
  `vt_createtime` varchar(50) DEFAULT NULL COMMENT '创建时间',
  `vt_parentid` varchar(50) DEFAULT NULL COMMENT '父编号',
  `vt_url` varchar(50) DEFAULT NULL COMMENT '链接'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频分类表';

-- 正在导出表  biubiubiu.videotype 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `videotype` DISABLE KEYS */;
/*!40000 ALTER TABLE `videotype` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
