-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.6.26 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 bz.privilege 结构
CREATE TABLE IF NOT EXISTS `privilege` (
  `privilege` int(11) NOT NULL AUTO_INCREMENT,
  `privilege_name` varchar(50) NOT NULL DEFAULT '0',
  `privilege_url` varchar(50) NOT NULL DEFAULT '0',
  `parent_id` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`privilege`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.privilege 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;


-- 导出  表 bz.puser 结构
CREATE TABLE IF NOT EXISTS `puser` (
  `puserlog_id` int(11) NOT NULL AUTO_INCREMENT,
  `puser_id` varchar(50) DEFAULT '0',
  `puserlog_url` varchar(50) DEFAULT '0',
  `puserlog_time` varchar(50) DEFAULT '0',
  PRIMARY KEY (`puserlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.puser 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `puser` DISABLE KEYS */;
/*!40000 ALTER TABLE `puser` ENABLE KEYS */;


-- 导出  表 bz.role 结构
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT '0',
  `role_describe` varchar(50) DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.role 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- 导出  表 bz.roleprivilege 结构
CREATE TABLE IF NOT EXISTS `roleprivilege` (
  `roleprivilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(50) DEFAULT '0',
  `privilege_id` varchar(50) DEFAULT '0',
  PRIMARY KEY (`roleprivilege_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.roleprivilege 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `roleprivilege` DISABLE KEYS */;
/*!40000 ALTER TABLE `roleprivilege` ENABLE KEYS */;


-- 导出  表 bz.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT '0',
  `user_password` varchar(50) DEFAULT '0',
  `user_phone` varchar(50) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- 导出  表 bz.userlog 结构
CREATE TABLE IF NOT EXISTS `userlog` (
  `userlog_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) DEFAULT '0',
  `userlog_url` varchar(50) DEFAULT '0',
  `userlog_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.userlog 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `userlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlog` ENABLE KEYS */;


-- 导出  表 bz.userrole 结构
CREATE TABLE IF NOT EXISTS `userrole` (
  `userrole_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) DEFAULT '0',
  `role_id` varchar(50) DEFAULT '0',
  PRIMARY KEY (`userrole_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  bz.userrole 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
