package com.ftg.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ServiceAppMain {

    public static void main(String[] args) {
        SpringApplication.run(ServiceAppMain.class,args);
    }

}