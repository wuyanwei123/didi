# t微信小程序 _万能墙设计文档

## 主体说明 

```
主语言 :layui ,java, mysql 
工具 ：jak1.8 、idea
```

## 界面

![](.\assets\capture_20190731111006529.bmp)

整体分为三个终端：



## 



## 后台表基本设计

### 用户表

| 表名 | user          |            |
| ---- | ------------- | :--------- |
| 编号 | 名称          | 备注       |
| 1    | user_id       | 用户编号   |
| 2    | user_name     | 用户名称   |
| 3    | user_password | 用户密码   |
| 4    | user_phone    | 用户手机号 |

### 角色表

| 表名 | role           |          |
| ---- | -------------- | -------- |
| 编号 | 名称           | 备注     |
| 1    | role_id        | 主键     |
| 2    | role_name      | 角色名称 |
| 3    | role _describe | 角色描述 |

### 权限表

| 表名 | privilege      |          |
| ---- | -------------- | -------- |
| 编号 | 名称           | 备注     |
| 1    | privilege_id   | 主键     |
| 2    | privilege_name | 权限名称 |
| 3    | privilege_url  | 权限链接 |
| 4    | parent_id      | 图片路径 |

### 用户登录记录表

| 表名 | userlog |  |
| ---- | ----------- | ---------- |
| 编号 | 名称        | 备注       |
| 1    | userlog_id | 主键       |
| 2    | user_id | 用户编号 |
| 3    | userlog_url | 用户访问连接 |
| 4 | userlog_time | 访问时间 |

### 会员登录记录表

| 表名 | puser         |              |
| ---- | ------------- | ------------ |
| 编号 | 名称          | 备注         |
| 1    | puserlog_id   | 主键         |
| 2    | puser_id      | 会员编号     |
| 3    | puserlog_url  | 会员访问连接 |
| 4    | puserlog_time | 访问时间     |

### 用户角色表

| 表名 | userrole    |        |
| ---- | ----------- | ------ |
| 编号 | 名称        | 备注   |
| 1    | userrole_id | 主键   |
| 2    | user_id     | 用户id |
| 3    | role_id     | 角色id |



### 角色权限表

| 表名 | roleprivilege    |        |
| ---- | ---------------- | ------ |
| 编号 | 名称             | 备注   |
| 1    | roleprivilege_id | 主键   |
| 2    | role_id          | 角色ID |
| 3    | privilege_id     | 权限ID |





## 

|      |      |      |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |



| 表名 | ys_role          | 角色表   |
| ---- | ---------------- | -------- |
| 编号 | 名称             | 备注     |
| 1    | role_id          | 主键     |
| 2    | role_name        | 角色名称 |
| 3    | role_description | 角色描述 |
| 4    | role_createtime  | 创建时间 |
|      |                  |          |
|      |                  |          |
|      |                  |          |
|      |                  |          |
|      |                  |          |

| 表名 | ys_user_role | 用户角色表 |
| ---- | ------------ | ---------- |
| 编号 | 名称         | 备注       |
| 1    | user_role_id | 主键       |
| 2    | user_id      | userID     |
| 3    | role_id      | role_id    |
|      |              |            |
|      |              |            |
|      |              |            |
|      |              |            |
|      |              |            |



| 表名 | ys_privilege       | 权限表 |
| ---- | ------------------ | ------ |
| 编号 | 名称               | 备注   |
| 1    | privilege_id       | 主键   |
| 2    | privilege_name     | 名称   |
| 3    | privilege_url      | url    |
| 4    | privilege_parentid | 父id   |
| 5    |                    |        |
|      |                    |        |
|      |                    |        |
|      |                    |        |



| 表名 | ys_privilege_role | 权限角色表  |
| ---- | ----------------- | ----------- |
| 编号 | 名称              | 备注        |
| 1    | privilege_role_id | 主键        |
| 2    | privilege_id      | privilegeID |
| 3    | role_id           | roleID      |
|      |                   |             |
|      |                   |             |
|      |                   |             |
|      |                   |             |
|      |                   |             |

## 交互方法说明

| 方法名称                                                     | 参数列表                                                     | 类型 | 说明                               |
| :----------------------------------------------------------- | ------------------------------------------------------------ | ---- | ---------------------------------- |
| /showMess                                                    | 无                                                           | get  | 用于]获取全部消息的方法，          |
| /showMess/{id}                                               | id消息主键                                                   | put  | 用于获取指定主键对应的消息         |
| /showPerson                                                  | 无                                                           | get  | 显示全部人员信息。                 |
| /showPerson/{id}                                             | id人员主键                                                   | put  | 指定人员主键                       |
| /showAdvice                                                  | 无                                                           | get  | 获取全部广告                       |
| /showChatinfo/{person}/{type}/{id}                           | person:人员  type：                                              学校、地区等 id：人员主键 | put  | 获取当前人、当前选择区域的聊天信息 |
| /publishMessage/{mname,mcontent, mpublisher,maccess}         | mname:标题  mcontent:内容  mpublisher:出版者maccess:可见权限 | post | 发布消息                           |
| /checkPerson/{uname,uidcard,utype}                           | uname:用户姓名 uidcard:身份证 utype:类型                     | post | 审核人员，通过：改变审核状态       |
| /addAdvice/{adtitle,adimgpath, adpath,adcontent,endtime,money} | adtitle:广告名称  adimgpath:广告图片 adpath:外链地址，外部应用网址      adcontent:文字说明，无adpath:时生效Launcher:广告投放者 endtime:结束时间 money:金额 | post | 添加广告信息                       |
| /addChatinfo/{uid,pushtype,pushcontent, mid}                 | uid发布者编号,pushtype发布类型,pushcontent内容，mid群组id    | post | 按钮点击发送消息                   |
| /addGroup/{groupname,groupphoto}                             | groupname:群昵称，groupphoto群头像                           | post | 添加群信息                         |
